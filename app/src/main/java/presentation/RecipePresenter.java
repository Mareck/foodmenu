package presentation;

import java.util.List;

import domain.GetRecipes;
import domain.RecipeView;
import entity.Recipe;

/**
 * Created by User on 2017-12-02.
 */

public class RecipePresenter {

    RecipeView recipeView;

    public void onAttach(RecipeView recipeView){
        this.recipeView = recipeView;
    }
    public void onDetach(){
        recipeView = null;
    }

    public List<Recipe> showRecipes(boolean isWithMeat){
        return GetRecipes.getRecipes(isWithMeat);
    }
}
