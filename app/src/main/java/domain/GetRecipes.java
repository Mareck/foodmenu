package domain;

import java.util.ArrayList;
import java.util.List;

import entity.Recipe;
import repository.RecipeReposotory;

/**
 * Created by User on 2017-11-28.
 */

public class GetRecipes {
    public static List<Recipe> getRecipes(boolean isWithMeat){
        List<Recipe> data = RecipeReposotory.GetAll();
        List<Recipe> result = new ArrayList<Recipe>();
       for (int i=0;i<data.size();i++){
           if (data.get(i).ContainsMeet == isWithMeat ){
               result.add(data.get(i));
           }
       }
       return result;
    }
    }
