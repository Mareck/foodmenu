package domain;

import java.util.List;

import entity.Recipe;

/**
 * Created by User on 2017-12-02.
 */

public interface RecipeView {
    void getRecipes( boolean isWithMeat);
}
